package com.piotrwdowiak.playground.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShoppingListController {

    @GetMapping("/list")
    public String getShoppingList() {
        return "hello spring";
    }

}
